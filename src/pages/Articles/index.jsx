import { useHistory } from 'react-router-dom';
import { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { getArticles, retrieveArticles } from '../../store/articles';
import Auth from "../../hoc/auth";
import ReactPaginate  from 'react-paginate';
import ArticlePreview from '../../components/ArticlePreview';
import '../../components/Pagination/index.css';

const Articles = () => {
    const history = useHistory();
    const dispatch = useDispatch();
    const articles = useSelector(getArticles);

    // Pagination
    const [currentPage, setCurrentPage] = useState(0);
    const PER_PAGE = 3;
    const offset = currentPage * PER_PAGE;

    const currentPageData = articles
        .slice(offset, offset + PER_PAGE)
        .map((article, index) => <ArticlePreview key={index} article={article}/>)
    const pageCount = Math.ceil(articles.length / PER_PAGE);

    const handlePageClick = ({ selected: selectedPage }) => {
        setCurrentPage(selectedPage);
    }

    useEffect(() => {
        if (!articles.length) {
          dispatch(retrieveArticles());
        }
    })

    return (
        <div className="w-full flex justify-center items-center flex-col p-32">
            <h1 className="text-center text-4xl font-bold">Liste des articles</h1>
            <button className="mt-5 mb-5 bg-green-500 rounded px-5 py-2 ml-5" onClick={() => history.push('/createArticle')} >Créer un article</button>
            <div className="w-full flex justify-center items-center flex-wrap">
                {currentPageData}
            </div>
            <ReactPaginate
                previousLabel={"prev"}
                nextLabel={"next"}
                breakLabel={"..."}
                breakClassName={"break-me"}
                pageCount={pageCount}
                marginPagesDisplayed={2}
                pageRangeDisplayed={5}
                onPageChange={handlePageClick}
                containerClassName={"pagination"}
                subContainerClassName={"pages pagination"}
                activeClassName={"active"}
            />
        </div>
    )
}

export default Auth(Articles);