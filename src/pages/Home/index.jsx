import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Auth from "../../hoc/auth";
import { getArticles, retrieveArticles } from "../../store/articles";
import ArticlePreview from '../../components/ArticlePreview';
import './style.css';

const Home = () => {
    const dispatch = useDispatch();
    const articles = useSelector(getArticles);
    function compare(a, b) {
        // Use toUpperCase() to ignore character casing
        const dateA = new Date(a.date);
        const dateB = new Date (b.date);
      
        let comparison = 0;
        if (dateA > dateB) {
          comparison = 1;
        } else if (dateA < dateB) {
          comparison = -1;
        }
        return comparison;
      }
    const newArray = articles.slice().sort(compare);
    const LastItems = newArray.slice(1).slice(-5);

      
    
    useEffect(() => {
        if (!articles.length) {
          dispatch(retrieveArticles());
        }
    })
    return (
        <div className="w-full flex justify-center items-center">
            <div className="flex flex-col items-center content-home">
            <h1 className="text-center text-4xl font-bold">Voici les 3 articles récents :</h1>
            <div className="content-articles" >
                {LastItems.map((article, index) => <ArticlePreview key={index} article={article}/>)}
            </div>
            </div>
        </div>
    )
}

export default Auth(Home)