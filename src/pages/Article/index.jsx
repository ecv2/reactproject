import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router';
import ArticleDescription from '../../components/ArticleDescription'
import Auth from "../../hoc/auth"
import { getArticle, retrieveArticles } from '../../store/articles';

const Article = () => {
    let { id } = useParams();
        const dispatch = useDispatch();        
        const article = useSelector(state => getArticle(state, parseFloat(id)));
        useEffect(() => {
            if (!article) {
                dispatch(retrieveArticles())
            }
        });
    return (
        <ArticleDescription article={article} />
    )
}

export default Auth(Article);
