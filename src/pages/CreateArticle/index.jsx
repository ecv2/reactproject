import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { getCurrentUser } from "../../store/users";
import { createArticle } from '../../store/articles';
import Auth from "../../hoc/auth";
import Button from '../../components/Button';
import Textarea from '../../components/Textarea';
import Input from '../../components/Input';
import Banner from '../../components/Banner';

const CreateArticle = () => {
    const user = useSelector(getCurrentUser);

    const dispatch = useDispatch();
    const history = useHistory();
    const [displayBanner, setDisplayBanner] = useState(false);
    const [fields, setFields] = useState({
        name: '',
        articleImage: '',
        description: '',
        price: '0.00',
        date: '',
    });

    const handleChangeField = ({ target: { name, value } }) => {
        setFields({ 
          ...fields, 
          [name]: value
        })
    };

    const closeBanner = () => setDisplayBanner(false);

    const submitForm = (e) => {
        e.preventDefault();
        const checkRequired = Object.keys(fields).find(key => fields[key] === '')
        if (checkRequired || fields.price === '0.00') {
          return;
        }
        dispatch(createArticle({
            ...fields,
            userId: user.id
        }))
        setDisplayBanner(true)
        setFields({
            name: '',
            articleImage: '',
            description: '',
            price: '0.00',
            date: '',
        })
        history.push('/articles');
    }
  
    return (
        <div className="w-full h-screen flex flex-col justify-center items-center px-10">
            <h1 className="text-center text-4xl font-bold">Créer un article</h1>
            <form onSubmit={submitForm} className="w-2/3 mx-auto">
            <Button text="Retour à la liste d'articles" handleClick={() => history.push('/articles')} />
                {displayBanner && <Banner text="Article ajoute !" close={closeBanner} />}
                <Input 
                label="Nom"
                id="name"
                name="name"
                className="mt-10"
                value={fields.name}
                handleChange={handleChangeField}
                />
                <Input 
                label="Image (url)" 
                id="articleImage" 
                name="articleImage" 
                value={fields.articleImage} 
                handleChange={handleChangeField} 
                />
                <Textarea 
                label="Description"
                id="description"
                name="description"
                value={fields.description}
                handleChange={handleChangeField}
                />      
                <Input 
                label="Prix"
                id="price"
                name="price"
                step="0.01"
                type="number"
                value={fields.price}
                handleChange={handleChangeField}
                />
                <Input
                label="Date de création"
                type="date"
                id="date"
                name="date"
                value={fields.date}
                handleChange={handleChangeField}
                />
                <Button type="submit" text="Créer l'article" />
            </form>
        </div>
    )
}

export default Auth(CreateArticle);