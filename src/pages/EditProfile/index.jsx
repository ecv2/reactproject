import Auth from "../../hoc/auth";
import EditForm from "../../components/EditForm";

const EditProfile = () => (  
    <div className="w-full h-screen flex justify-center items-center">
        <div className="w-2/3">
            <p>Edition Profil</p>
            <EditForm />
        </div>
    </div>
)

export default Auth(EditProfile);