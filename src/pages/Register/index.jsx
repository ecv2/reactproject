import { Link } from "react-router-dom";
import Auth from "../../hoc/auth";
import RegisterForm from "../../components/RegisterForm";

  const Register = () => (
    <div className="w-full h-screen flex justify-center items-center">
            <div className="w-2/3">
            <RegisterForm/>
            <p className="text-center"><Link to="/login">Deja inscrit ?</Link></p>
            </div>
        </div>
    )

export default Auth(Register);