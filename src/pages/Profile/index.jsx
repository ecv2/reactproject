import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { getCurrentUser } from "../../store/users";
import Auth from "../../hoc/auth";

const Profile = () => {  
    const user = useSelector(getCurrentUser);

    return(
        <div className="w-full h-screen flex justify-center items-center px-10">
        <div className="w-1/3">
            <img src={user.profilePicture} alt=""/>
        </div>
        <div className="w-2/3">
            <p>{ user.firstName } { user.lastName }</p>
            <p>{ user.email }</p>
            <Link className="py-2 px-5 my-1 text-white bg-gray-400 shadow-sm rounded" to="/editProfile">Éditer mon profil</Link>
        </div>
    </div>
    )
}

export default Auth(Profile);