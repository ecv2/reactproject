import { Link } from "react-router-dom";
import Auth from "../../hoc/auth";
import LoginForm from "../../components/LoginForm";

const Login = () => (
<div className="w-full h-screen flex justify-center items-center">
    <div className="w-2/3">
        <LoginForm/>
        <p className="text-center"><Link to="/register">Pas encore inscrit ?</Link></p>
    </div>
</div>
)

export default Auth(Login);