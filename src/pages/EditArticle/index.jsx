import { useEffect } from 'react';
import { useParams, useHistory  } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { getArticle, retrieveArticles } from '../../store/articles';
import Auth from "../../hoc/auth";
import EditArticleForm from "../../components/EditArticleForm";
import Button from '../../components/Button';

const EditArticle = () => {
  let { id } = useParams();
  const dispatch = useDispatch();
  const article = useSelector(state => getArticle(state, parseFloat(id)));
  const history = useHistory();

  useEffect(() => {
    if (!article) {
      dispatch(retrieveArticles())
    }
  });

  return (
    <div className="w-full flex justify-center items-center flex-col p-32">
        <div className="w-2/3 flex flex-col items-center justify-center">
            <h1 className="text-center mb-5 text-4xl font-bold">Modification de l'article</h1>
            <Button text="Retour à la liste d'articles" handleClick={() => history.push('/articles')} />
            <>
              {article && <EditArticleForm article={article} />}
            </>
        </div>
    </div>
  )
}

export default Auth(EditArticle);