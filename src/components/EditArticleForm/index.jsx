import { useState } from 'react'
import { useHistory } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { updateArticle, editArticle } from '../../store/articles'

import Button from '../../components/Button'
import Textarea from '../../components/Textarea'
import Input from '../../components/Input'
import { getCurrentUser } from '../../store/users';

const EditArticleForm = ({article}) => {
    const dispatch = useDispatch();
    const history  = useHistory();
    const user = useSelector(getCurrentUser);
    const [fields, setFields] = useState({
        name: article.name,
        articleImage: article.articleImage,
        description: article.description,
        price: article.price,
        date: article.date,
        id: article.id,
        userId: user.id
    })

    const handleChangeField = ({ target: { name, value } }) => {
        setFields({ 
          ...fields, 
          [name]: value
        })
    };

    const submitForm = (e) => {
        e.preventDefault();
        const checkRequired = Object.keys(fields).find(key => fields[key] === '')
        if (checkRequired || fields.price === '0.00') {
          return;
        }
        dispatch(updateArticle(fields))
        dispatch(editArticle(fields))
        history.push(`/article/${article.id}`)
    }
  
    return (
        <div className="w-full flex justify-center items-center px-10">
            <form onSubmit={submitForm} className="w-2/3 mx-auto">
                <Input 
                label="Nom"
                id="name"
                name="name"
                className="mt-10"
                value={fields.name}
                handleChange={handleChangeField}
                />
                <Input 
                label="Image (url)" 
                id="articleImage" 
                name="articleImage" 
                value={fields.articleImage} 
                handleChange={handleChangeField} 
                />
                <Textarea 
                label="Description"
                id="description"
                name="description"
                value={fields.description}
                handleChange={handleChangeField}
                />      
                <Input 
                label="Prix"
                id="price"
                name="price"
                step="0.01"
                type="number"
                value={fields.price}
                handleChange={handleChangeField}
                />
                <Input
                label="Date de Mise à jour"
                id="date"
                type="date"
                name="date"
                value={fields.date}
                handleChange={handleChangeField}
                />
                <Button type="submit" text="Modifier l'article" />
            </form>
        </div>
    )
}

export default (EditArticleForm);