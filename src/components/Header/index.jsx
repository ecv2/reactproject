import Cookies from 'js-cookie'
import { useSelector, useDispatch } from 'react-redux'
import { useHistory, Link } from 'react-router-dom';
import { getCurrentUser, resetProfile } from '../../store/users'
import "./index.css";

function Header () {
    const user = useSelector(getCurrentUser);
    const history = useHistory();
    const dispatch = useDispatch();

    const logout = () => {
        dispatch(resetProfile())
        Cookies.remove('jwt')
        history.push('/login')
    }

    return(
        <header className="w-full h-20 px-5 flex flex-col items-center justify-center bg-gray-600">
            <nav className="w-full flex items-center justify-between flex-wrap">
                <ul className="w-full flex items-center">
                {user === null ? (
                        <li className="mr-6">
                            <button 
                            className="inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-gray-500 hover:bg-white lg:mt-0"
                            onClick={() => history.push('/login')}
                            >Se connecter</button>
                        </li>
                    ) : (
                    <>
                        <li className="mr-6">
                            <Link className="text-white hover:text-grey-800" to="/">Home</Link>
                        </li>
                        <li className="mr-6">
                            <Link className="text-white hover:text-grey-800" to="/profile">Profil</Link>
                        </li>
                        <li className="mr-6">
                            <Link className="text-white hover:text-grey-800" to="/articles">Articles</Link>
                        </li>
                        <li className="mr-6">
                            <button 
                            className="inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-gray-500 hover:bg-white lg:mt-0"  
                            onClick={logout}>
                            Deconnexion
                            </button>
                        </li>
                    </>
                )}
                </ul>
            </nav>
        </header>
    )   
}

export default Header