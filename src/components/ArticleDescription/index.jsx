import { Link, useHistory } from "react-router-dom";
import { getCurrentUser } from "../../store/users";
import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { removeArticleById } from '../../store/articles';
import PropTypes from 'prop-types';
import Button from '../Button';
import Select from '../Select';

const ArticleDescription = ({ article }) => {
  const history = useHistory();
  const dispatch = useDispatch();
  const user = useSelector(getCurrentUser);
  const [isAuthor, setIsAuthor] = useState(false)
  const [fields, setFields] = useState({
    quantity: ''
  });

  const quantity = [
    {label: '0', value: null},
    {label: '1', value: 1},
    {label: '2', value: 2},
    {label: '3', value: 3},
    {label: '4', value: 4},
    {label: '5', value: 5}
  ];

 useEffect(() => {
    if (article.userId === user.id) {
      setIsAuthor(true)
    }
  }, []);

  const deleteArticle = () => {
    dispatch(removeArticleById(article.id))
    history.push('/articles')
  };

  const handleChangeField = ({ target: { name, value } }) => {
    setFields({ 
      ...fields, 
      [name]: value
    })
  };

  return (
    <div className="w-full flex justify-center items-center flex-col p-32">
      <Button className="absolute" text="Retour à la liste articles" handleClick={() => history.push('/articles')} />
      <h1 className="mt-10 mb-10 text-center font-semibold text-4xl">Titre: {article.name}</h1>
      <div>Image: <img src={article.articleImage} alt=""/></div>
      <p>Description: {article.description}</p>
      <p>Prix: {article.price} €</p>
      <Select
        label="Quantité: "
        options={quantity}
        name="quantity"
        id="quantity"
        value={fields.quantity}
        handleChange={handleChangeField}
        />
        {isAuthor? 
        <>
        <Link className="mt-5 mb-5 bg-blue-300 rounded px-5 py-2" to={`/editArticle/${article.id}`}>Modifier l'article</Link>
        <button className="mt-5 mb-5 bg-red-500 rounded px-5 py-2" onClick={(deleteArticle)}>Supprimer l'article</button>
        </>
        : ''
      }
    </div>
  );
}

ArticleDescription.propTypes = {
  article: PropTypes.shape({
    id: PropTypes.number.isRequired,
    price: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    articleImage: PropTypes.string.isRequired,
  })
};

export default ArticleDescription;
