import './index.css';
function Footer () {
    return(
        <footer className="w-full h-10 px-5 flex flex-col items-end justify-center bg-gray-600">
            <p>2020© Manida Vilay & Guilhem Fouassier</p>
        </footer>
    )   
}

export default Footer