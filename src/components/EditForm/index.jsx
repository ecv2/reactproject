import { editProfile, editUser, getCurrentUser } from '../../store/users';
import { useSelector, useDispatch } from 'react-redux';
import { useState } from 'react';
import { useHistory } from 'react-router-dom';
import Input from '../Input';
import Button from '../Button';

const EditForm = () => {
    const user = useSelector(getCurrentUser);
    const dispatch = useDispatch();
    const history = useHistory();
    let valuePicture = '';
    if( user.profilePicture !== null){
        valuePicture = user.profilePicture
        console.log(user)
    }else{
        console.log('aaa')
        valuePicture = ''
    }
    const [fields, setFields] = useState({
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        password: user.password,
        profilePicture : valuePicture,
        id: user.id
    });

  const handleChangeField = ({ target: { name, value } }) => setFields({ ...fields, [name]: value });
  const submitForm = async e => {
    e.preventDefault();

    const checkError = Object.keys(fields).find(field => field === '')

    if (checkError) {
      return;
    }
    
    dispatch(editProfile(fields))
    dispatch(editUser(fields))
    history.push("/profile")
  };
  
  return (
    <>
      <form onSubmit={submitForm} className="m-auto border rounded p-5">
        <Input label="Picture ( url )" id="profilePicture" name="profilePicture" value={fields.profilePicture} handleChange={handleChangeField} />
        <Input label="Prénom" id="firstName" name="firstName" value={fields.firstName} handleChange={handleChangeField} />
        <Input label="Nom" id="lastName" name="lastName" value={fields.lastName} handleChange={handleChangeField}  />
        <Input label="Identifiant" type="email" id="email" name="email" value={fields.email} handleChange={handleChangeField}  />
        <Input label="Mot de passe" id="password" type="password" name="password" value={fields.password} handleChange={handleChangeField}  />
        <Button type="submit" text="Editer mon profil"/>
      </form>
    </>
  )
}

export default EditForm;