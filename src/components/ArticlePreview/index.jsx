import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';
import Button from '../Button';

const ArticlePreview = ({ article }) => {
  const history = useHistory();

  return (
    <div className="w-1/4 m-6 flex flex-col items-center py-5 h-50 border">
      <p className="text-center">{article.name}</p>
      <p className="text-center"><img src={article.articleImage} alt=""/></p>
      <p className="text-center">{article.description}</p>
      <p className="text-center">{article.price}</p>
      <p className="text-center">{article.date}</p>
      <Button className="" text="Consulter l'article" handleClick={() => history.push(`/article/${article.id}`)} />
    </div>
  )
}

ArticlePreview.propTypes = {
  article: PropTypes.shape({
    id: PropTypes.number.isRequired,
    price: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    articleImage: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired
  })
};

export default ArticlePreview;