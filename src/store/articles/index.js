import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  list: []
}

const articles = createSlice({
  name: 'articles',
  initialState,
  reducers: {
    addArticle(state, { payload }) {
      state.list = [...state.list, payload]
    },
    addArticles(state, { payload }) {
      state.list = payload
    },
    editArticle(state, { payload }) {
      state.list = [...state.list.map(item => item.id === payload.id ? payload : item  )]
    },
    removeArticle(state, { payload }) {
      state.list = [...state.list.filter(item => item.id !== payload.id)]
    },
  }
});

// Actions
export const {
  addArticle,
  addArticles,
  editArticle,
  removeArticle
} = articles.actions;

// Redux-thunk
export const createArticle = article => async dispatch => {
  try {
    const response = await fetch(`${process.env.REACT_APP_API_URL}articles`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(article)
    })
    const data = await response.json();
    dispatch(addArticle(data))
  } catch (e) {
    console.error(e);
  }
}

export const retrieveArticles = () => async dispatch => {
  try {
    const response = await fetch(`${process.env.REACT_APP_API_URL}articles`, {
      method: 'GET',
    })
    const data = await response.json();
    dispatch(addArticles(data))
  } catch (e) {
    console.error(e);
  }
}

// Redux-thunk
export const updateArticle = article => async dispatch => {
  try {
    const response = await fetch(`${process.env.REACT_APP_API_URL}articles/${article.id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(article)
    })
    await response.json();
  } catch(e) {
    console.error(e);
  }
}

export const removeArticleById = id => async dispatch => {
  try {
    await fetch(`${process.env.REACT_APP_API_URL}articles/${id}`, {
      method: 'DELETE'
    })
    dispatch(removeArticle({
      id
    }))
  } catch (e) {
    console.error(e);
  }
}

// Selectors
export const getArticles = (state) => state.articles.list;

export const getArticle = (state, id) => state.articles.list.find(article => article.id === id);

export default articles.reducer;