import { combineReducers } from 'redux';
import { configureStore } from '@reduxjs/toolkit';
import thunk from 'redux-thunk';
import users from './users';
import articles from './articles';

const reducer = combineReducers({
    users,
    articles
})

const store = configureStore({ reducer, devTools: true, middleware: [thunk] });

export default store;
