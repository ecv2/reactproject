import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom'
import Home from './pages/Home';
import Register from './pages/Register';
import Footer from './components/Footer';
import Login from './pages/Login';
import './tailwind.output.css'
import Profile from './pages/Profile';
import EditProfile from './pages/EditProfile';
import Articles from './pages/Articles';
import CreateArticle from './pages/CreateArticle';
import Article from './pages/Article';
import EditArticle from './pages/EditArticle';

function App() {
  return (
    <>
    <Router>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/register" component={Register} />
        <Route path="/login" component={Login} />
        <Route path="/profile" component={Profile} />
        <Route path="/editProfile" component={EditProfile} />
        <Route path="/articles" component={Articles} />
        <Route path="/createArticle" component={CreateArticle} />
        <Route path="/article/:id" component={Article} />
        <Route path="/editArticle/:id" component={EditArticle}/>
      </Switch>
      <Footer/>
    </Router>
    </>
  );
}

export default App;
